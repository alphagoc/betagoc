# -*- coding:utf-8 -*-

import pandas as pd

class EmbeddingAgent(object):
    def __init__(self, fileId):
        self.inputHeader = 'inputdistancetime'
        self.outputHeader = 'network'
        self.FileId= fileId
        self.inputFile = self.inputHeader + '_' + self.FileId + '.txt'
        self.outpuFile = self.outpuFile + '_' + self.FileId
        self.vec_1st_file = 'vec_1s_' + fileId
        self.vec_2nd_file =  'vec_2nd_' + fileId
        self.vec_all_file = 'vec_all_' + fileId

        self.vec_1st_file_handle = self.vec_1s_file + 'handle'
        self.vec_2nd_file_handle = self.vec_2nd_file + 'handle'
        self.vec_all_file_handle = self.vec_all_file + 'handle'

    def handleFile(self):
        # vec_1s_inputFile = open(self.vec_1s_file, 'r')
        #
        # vec_2nd_inputFile = open(self.vec_2nd_file, 'r')
        # vec_all_inputFile = open(self.vec_all_file, 'r')
        #
        # vec_1s_inputFile_handle = open(self.vec_1s_file_handle, 'w')
        # vec_2nd_inputFile_handle = open(self.vec_2nd_file_handle, 'w')
        # vec_all_inputFile_handle = open(self.vec_all_file_handle, 'w')

        with open(self.vec_1st_file_handle, 'w') as vec_1st_inputFile_handle:
            flag = True
            with open(self.vec_1st_file, 'r') as vec_1st_inputFile:
                flag = True
                for line in vec_1st_inputFile:
                    if flag:
                        flag = False
                    else:
                        vec_1st_inputFile_handle.write(line)

        with open(self.vec_2nd_file_handle, 'w') as vec_2nd_inputFile_handle:
            flag = True
            with open(self.vec_2nd_file, 'r') as vec_2nd_inputFile:
                flag = True
                for line in vec_2nd_inputFile:
                    if flag:
                        flag = False
                    else:
                        vec_2nd_inputFile_handle.write(line)

        with open(self.vec_all_file_handle, 'w') as vec_all_inputFile_handle:
            flag = True
            with open(self.vec_all_file, 'r') as vec_all_inputFile:
                flag = True
                for line in vec_all_inputFile:
                    if flag:
                        flag = False
                    else:
                        vec_all_inputFile_handle.write(line)


    def getEmbedding_1st(self,locId):
        data = pd.read_table(self.vec_1st_file_handle, sep=' ', header=None)
        ourdata = data.drop([128], axis=1)
        result = ourdata.iloc[[locId]].values

    def getEmbedding_2nd(self, locId):
        data = pd.read_table(self.vec_2nd_inputFile_handle, sep=' ', header=None)
        ourdata = data.drop([128], axis=1)
        result = ourdata.iloc[[locId]].values

    def getEmbedding_all(self, locId):
        data = pd.read_table(self.vec_all_inputFile_handle, sep=' ', header=None)
        ourdata = data.drop([256], axis=1)
        result = ourdata.iloc[[locId]].values







