#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: policy_value_network
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:8/16/18,8:57 PM
#==================================

import numpy as np
import ipdb
import util
import tensorflow as tf
from env_mcts import convert_state2vector
from environment import *

class policy_value_network(object):
    def __init__(self, config, variable_scope = "target", trainable = True):
        self.config = config
        self.variable_scope = variable_scope
        self.trainable = trainable
        with tf.Graph().as_default():
            self._build_model()
            self.sess = tf.Session(config = tf.ConfigProto(gpu_options=config.GPU_OPTION))
            self.saver = tf.train.Saver(max_to_keep=5000)
            self.sess.run(self.init)

    def _build_model(self):
        with tf.variable_scope(self.variable_scope):
            self._create_placeholders()
            self._create_global_step()
            self._create_inference()
            if self.trainable:
                self._create_optimizer()
            self._create_intializer()

    def _create_placeholders(self):
        self.state = tf.placeholder(tf.float32, (None,self.config.action_size,self.config.state_size))
        self.mcts_policy = tf.placeholder(tf.float32,(None,self.config.action_size))
        self.expected_reward = tf.placeholder(tf.float32,(None,1))
        self.learning_rate = tf.placeholder(tf.float32)

    def _create_global_step(self):
        self.global_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_step')

    def _create_intializer(self):
        with tf.name_scope("initlializer"):
            self.init = tf.global_variables_initializer()

    def _create_inference(self):
        # dense_state
        weight = tf.Variable(np.random.uniform(-0.001,0.001,(self.config.state_size,10)),
                             dtype=tf.float32, trainable=self.trainable, name='item_feature')
        dense_state = tf.tensordot(self.state,weight,axes=[[2],[0]])
        dense_state = tf.reshape(dense_state,(-1,10*self.config.action_size))
        self.action_fc = tf.layers.dense(inputs=dense_state,
                                         units=self.config.action_size,
                                         activation=tf.nn.log_softmax,
                                         trainable=self.trainable)
        self.value_fc = tf.layers.dense(inputs =dense_state,
                                        units=1,
                                        activation=None,
                                        trainable=self.trainable)

    def _create_optimizer(self):
        self.value_loss = tf.losses.mean_squared_error(self.value_fc,
                                                       self.expected_reward)
        self.action_loss = tf.negative(tf.reduce_mean(
                tf.reduce_sum(tf.multiply(self.mcts_policy, self.action_fc), 1)))
        l2_penalty_beta = 1e-4
        vars = tf.trainable_variables()
        l2_penalty = l2_penalty_beta * tf.add_n(
            [tf.nn.l2_loss(v) for v in vars if 'bias' not in v.name.lower()])
        self.loss = self.value_loss + self.action_loss + l2_penalty
        self.optimizer = tf.train.AdamOptimizer(
                learning_rate=self.learning_rate).minimize(self.loss)

    def policy_value_fn(self,state):
        state_emb = convert_state2vector(state,self.config.action_size).reshape((1,self.config.action_size,-1))
        action_value,value_fc = self.sess.run([self.action_fc,self.value_fc],feed_dict={self.state:state_emb})
        action_value = np.exp(action_value)
        actions = []
        for item in get_actions(state):
            actions.append((item,action_value[0,item]))
        return actions,value_fc[0][0]

    def batch_policy_value_fn(self,state):
        state_emb = np.concatenate([convert_state2vector(item,self.config.action_size).reshape((1,self.config.action_size,-1)) for item in state])
        action_value,value_fc = self.sess.run([self.action_fc,self.value_fc],feed_dict={self.state:state_emb})
        action_value = np.exp(action_value)
        return action_value,value_fc

    def optimize_model(self,state,mcts_policy,expected_reward,learning_rate):
        state = np.concatenate([convert_state2vector(item,self.config.action_size).reshape((1,self.config.action_size,-1))  for item in state],axis=0)
        mcts_policy = np.concatenate([item.reshape((1,-1)) for item in mcts_policy],axis=0)
        expected_reward = np.asarray(expected_reward).reshape((-1,1))
        return self.sess.run([self.loss,self.value_loss,self.action_loss,self.optimizer],feed_dict={self.state:state,
                                                                   self.mcts_policy:mcts_policy,
                                                                   self.expected_reward:expected_reward,
                                                                   self.learning_rate:learning_rate})[:-1]

