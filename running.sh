#!/usr/bin/env bash
### dataID 01
declare -i no_gpu=1
declare -i action_size=1601
declare -i state_size=36
#~/pyenv3/bin/python3 ./train.py -log_path logs -model car_zero -no_gpu $no_gpu -training_epoch 2000 -batch_size 100 \
#-buffer_size 20000 -state_size $state_size -action_size $action_size -simulation_num 1 -temperature 1.0 -n_playout 1000 \
#-c_puct 1000 -discount_factor 0.9 -learning_rate 0.01 -kl_targ 0.02 -mini_epoch 10 -lr_multiplier 0.1 -simulator 2 \
#-task train -saved_model_path ./saved_model/ -car_type 0

#~/pyenv3/bin/python3 ./train.py -log_path logs -model car_one -no_gpu $no_gpu -training_epoch 2000 -batch_size 100 \
#-buffer_size 20000 -state_size $state_size -action_size $action_size -simulation_num 1 -temperature 1.0 -n_playout 1000 \
#-c_puct 1000 -discount_factor 0.9 -learning_rate 0.01 -kl_targ 0.02 -mini_epoch 10 -lr_multiplier 0.1 -simulator 2 \
#-task train -saved_model_path ./saved_model/ -car_type 1

~/pyenv3/bin/python3 ./train.py -log_path logs -model test_model -no_gpu $no_gpu -training_epoch 2000 -batch_size 100 \
-buffer_size 20000 -state_size $state_size -action_size $action_size -simulation_num 1 -temperature 1.0 -n_playout 1 \
-c_puct 1000 -discount_factor 0.9 -learning_rate 0.01 -kl_targ 0.02 -mini_epoch 10 -lr_multiplier 0.1 -simulator 2 \
-task evaluate -saved_model_path ./saved_model/ -car_one_save_path car_one_0 -car_zero_save_path car_zero_0
