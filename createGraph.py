# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import sys
sysLen = len(sys.argv)
if sysLen != 2:
    print('Please read ReadMe')
    exit()
else:
    inputFile = sys.argv[1]

# inputFileHeader = 'inputdistancetime'
outputFileHeader = 'network'
inputFile = 'inputdistancetime_5_1201.txt'
input_paramer = inputFile.split('_')
outputFile = outputFileHeader + '_' + input_paramer[1]+'_'+input_paramer[2]
data_DisTime = pd.read_table(inputFile, sep=',')
# print(data_DisTime)

# data_shape = data_DisTime.shape
# print data_shape

total_rows = len(data_DisTime.index)
print total_rows

col_from_node = data_DisTime.iloc[:, 1]
all_from_node = col_from_node.values
# print all_from_node
np_from_node = np.array(all_from_node)
unique_from_node = np.unique(np_from_node)
# unique_from_node = all_from_node.unique()
print unique_from_node
# total_rows = data_DisTime['ID'].count
# print total_rows

allGraph = pd.DataFrame()
for i in range(0, unique_from_node.size,1):
    id_from_node = unique_from_node[i]
    id_to_nodes = data_DisTime[data_DisTime['from_node']==id_from_node]
    id_to_nodes_sort = id_to_nodes.sort_values('distance')
    if len(id_to_nodes.index) > 20:
        # id_to_nodes_select_distance20 = id_to_nodes_sort.iloc[0:20, ]
        id_to_nodes_select_distance20 = id_to_nodes_sort.head(20)

        # id_to_nodes_distance20plus = id_to_nodes_sort.drop([0, 1, 2, 3])
        id_to_nodes_distance20plus = id_to_nodes_sort.tail(len(id_to_nodes.index)-20)
        # id_to_nodes_sort_time = id_to_nodes_distance20plus.sort_values('spend_tm')
        halfTime = 100
        id_to_nodes_select_timeHalf = id_to_nodes_distance20plus[id_to_nodes_distance20plus['spend_tm']<=halfTime]
        if len(id_to_nodes_select_timeHalf.index > 0):
            id_select = id_to_nodes_select_distance20.append(id_to_nodes_select_timeHalf)
        else:
            id_select = id_to_nodes_select_distance20
    else:
        id_select = id_to_nodes_sort

    partGraph = id_select.drop(columns=['ID', 'spend_tm'])
    allGraph = allGraph.append(partGraph)



allGraph.to_csv(outputFile, sep=" ", index=False, header=False)



