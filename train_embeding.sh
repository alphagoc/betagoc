#!/bin/sh

g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result line.cpp -o line -lgsl -lm -lgslcblas
g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result reconstruct.cpp -o reconstruct
g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result normalize.cpp -o normalize
g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result concatenate.cpp -o concatenate

python createGraph.py inputdistancetime_1_1601.txt
python createGraph.py inputdistancetime_2_1501.txt
python createGraph.py inputdistancetime_3_1401.txt
python createGraph.py inputdistancetime_4_1301.txt
python createGraph.py inputdistancetime_5_1201.txt




./reconstruct -train network_1_1601.txt -output network_dense_1_1601 -depth 2 -k-max 1000
./line -train network_dense_1_1601 -output vec_1st_wo_norm_1_1601 -binary 0 -size 128 -order 1 -negative 5 -samples 10000 -threads 40
./line -train network_dense_1_1601 -output vec_2nd_wo_norm_1_1601 -binary 0 -size 128 -order 2 -negative 5 -samples 10000 -threads 40
./normalize -input vec_1st_wo_norm_1_1601 -output vec_1st_1_1601 -binary 0
./normalize -input vec_2nd_wo_norm_1_1601 -output vec_2nd_1_1601 -binary 0
./concatenate -input1 vec_1st_1_1601 -input2 vec_2nd_1_1601 -output vec_all_1_1601 -binary 0





./reconstruct -train network_2_1501.txt -output network_dense_2_1501 -depth 2 -k-max 1000
./line -train network_dense_2_1501 -output vec_1st_wo_norm_2_1501 -binary 0 -size 128 -order 1 -negative 5 -samples 10000 -threads 40
./line -train network_dense_2_1501 -output vec_2nd_wo_norm_2_1501 -binary 0 -size 128 -order 2 -negative 5 -samples 10000 -threads 40
./normalize -input vec_1st_wo_norm_2_1501 -output vec_1st_2_1501 -binary 0
./normalize -input vec_2nd_wo_norm_2_1501 -output vec_2nd_2_1501 -binary 0
./concatenate -input1 vec_1st_2_1501 -input2 vec_2nd_2_1501 -output vec_all_2_1501 -binary 0



./reconstruct -train network_3_1401.txt -output network_dense_3_1401 -depth 2 -k-max 1000
./line -train network_dense_3_1401 -output vec_1st_wo_norm_3_1401 -binary 0 -size 128 -order 1 -negative 5 -samples 10000 -threads 40
./line -train network_dense_3_1401 -output vec_2nd_wo_norm_3_1401 -binary 0 -size 128 -order 2 -negative 5 -samples 10000 -threads 40
./normalize -input vec_1st_wo_norm_3_1401 -output vec_1st_3_1401 -binary 0
./normalize -input vec_2nd_wo_norm_3_1401 -output vec_2nd_3_1401 -binary 0
./concatenate -input1 vec_1st_3_1401 -input2 vec_2nd_1_3_1401 -output vec_all_1_1601 -binary 0



./reconstruct -train network_4_1301.txt -output network_dense_4_1301 -depth 2 -k-max 1000
./line -train network_dense_4_1301 -output vec_1st_wo_norm_4_1301 -binary 0 -size 128 -order 1 -negative 5 -samples 10000 -threads 40
./line -train network_dense_4_1301 -output vec_2nd_wo_norm_4_1301 -binary 0 -size 128 -order 2 -negative 5 -samples 10000 -threads 40
./normalize -input vec_1st_wo_norm_4_1301 -output vec_1st_4_1301 -binary 0
./normalize -input vec_2nd_wo_norm_4_1301 -output vec_2nd_4_1301 -binary 0
./concatenate -input1 vec_1st_4_1301 -input2 vec_2nd_4_1301 -output vec_all_4_1301 -binary 0


./reconstruct -train network_5_1201.txt -output network_dense_5_1201 -depth 2 -k-max 1000
./line -train network_dense_5_1201 -output vec_1st_wo_norm_5_1201 -binary 0 -size 128 -order 1 -negative 5 -samples 10000 -threads 40
./line -train network_dense_5_1201 -output vec_2nd_wo_norm_5_1201 -binary 0 -size 128 -order 2 -negative 5 -samples 10000 -threads 40
./normalize -input vec_1st_wo_norm_5_1201 -output vec_1st_5_1201 -binary 0
./normalize -input vec_2nd_wo_norm_5_1201 -output vec_2nd_5_1201 -binary 0
./concatenate -input1 vec_1st_5_1201 -input2 vec_2nd_5_1201 -output vec_all_5_1201 -binary 0
