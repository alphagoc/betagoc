#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: plot_training_results
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:8/21/18,9:22 AM
#==================================


import numpy as np
import ipdb

import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import copy as cp
import numpy as np
import ipdb
from matplotlib import pyplot as plt
from scipy import optimize
import brewer2mpl

# brewer2mpl.get_map args: set name set type number of colors
bmap = brewer2mpl.get_map('Set2','qualitative', 7)
colors = bmap.hex_colors
import matplotlib as mpl
import matplotlib.font_manager as fm
# mpl.rcParams['axes.color_cycle'] = colors
font = fm.FontProperties(size='medium')
font.set_weight('bold')
new_font = fm.FontProperties(size='x-small')
# adding breakpoint with ipdb.set_trace()
import math


import numpy as np
import ipdb

import numpy as np
from scipy import interpolate
from collections import OrderedDict
def read_results(path):
    res = []
    with open(path,"r") as f:
        for line in f.readlines():
            if str(line).__contains__("lr_multiplier") and str(line).__contains__("entropy"):
                res.append(str(line).strip("\n").split(","))
    res = list(zip(*res))
    datas =OrderedDict()
    for item in res:
        datas[item[0].split(":")[0]] = [float(v.split(":")[1]) for v in item]
    return datas


fig = plt.figure(1, figsize=(12, 3), tight_layout={'pad': 0.2, 'w_pad': 2.0, 'h_pad': 1.0})
results = read_results("log_18-20.txt")
for i,item in enumerate(results.items()):
    key,value = item
    plt.subplot2grid((2, 4), (int(i/4), i%4))
    plt.plot(range(len(value)),value)
    plt.ylabel(key)
    plt.title(key, fontproperties=font)
plt.show()
fig.savefig('./results.png', dpi=fig.dpi)


