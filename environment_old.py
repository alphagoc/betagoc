#!/usr/bin/python
# encoding: utf-8
# -*- coding:utf-8 -*-

#==================================
#@file name: environment
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:8/11/18,8:13 PM
#==================================
from __future__ import print_function

import numpy as np
import pandas as pd
import os
import re
from collections import OrderedDict

#import ipdb
def cleanData():
    rec = re.compile(r'inputnode*')
    dirName = './data'
    files = os.listdir(dirName)
    res = []
    for file in files:
        if rec.match(file):
            path = dirName + '/' + file
            node_type = np.array(pd.read_excel(path))
            for row in node_type:
                row = list(row)
                if row[-3] == '-':
                    row[-3] = 0.0
                    row[-4] = 0.0
                if row[-1] != '-':
                    s1 = row[-1].strftime('%H-%S').split('-')
                    s2 = row[-2].strftime('%H-%S').split('-')
                    row[-1] = int(s1[0])*60 + int(s1[1])
                    row[-2] = int(s2[0])*60 + int(s2[1])
                else:
                    row[-1] = 0
                    row[-2] = 0
                res.append(row)
    return res
            
class environment(object):
    
    action_number = 100
    waitCost = 0.4
    currentNode = 0
    
    remainNodes = 0
    finalReward = 0.0
    realCost = 0.0
    
    feature_index = {'nodeType':0, 'isDeliver':1, 'goods_volume':2, \
                     'goods_weight':3, 'cur_time':4, 'latest_time':5, \
                     'earliest_time':6, 'distance':7, 'remain_power':8, \
                     'arrivable':9,'arrived_remain_power':10, \
                     'remain_volume':11, 'remain_weight':12, 'embedding':13}
    car = [{'max_volume':12,'max_weight':2.0,'max_power':100000,'disCost':12,'carCost':200},\
           {'max_volume':16,'max_weight':2.5,'max_power':120000,'disCost':14,'carCost':300}]
    node_type_index = {'type':0,'weight':3,'volume':4,'earliest_time':5,'latest_time':6}
    

    
    def __init__(self,carID):
        self.carID = carID
        self.disCost = self.car[self.carID]['disCost'] / 1000
        self.input_distance_time = {}
        self.input_node_type = {}
        
        '''
        get head 20(self.action_number) for test
        you can use all data
        '''
        #=================================================
        with open('./data/inputdistancetime_5_1201.txt','r') as f:
            line = f.readlines()
            input_dis_time = line[1:(1+1200*self.action_number)]
            
        for line in input_dis_time:
            items = line.strip().split(',')
            (o, node_1, node_2, dis, time) = list(map(int,items))
            if node_1 not in self.input_distance_time:
                self.input_distance_time[node_1] = {}
            self.input_distance_time[node_1][node_2] = [dis, time]
            
        #=================================================
        #input_node_type = {}
        node_data = cleanData()
        for row in node_data:
            self.input_node_type[row[0]] = row[1:]
            
        self.nodes = self.input_distance_time.keys()
        
    def get_start_state(self):
        s_0 = OrderedDict()
        #s_0 = self.s_0
        for i in self.nodes:
            node_info = self.input_node_type[i]
            
            nodeType = node_info[self.node_type_index['type']] # 1:配送中心; 4:充电桩; 3:揽货商家 ; 2:送货商家(int)
            isDeliver = 0 #是否配送过 (int)
            goods_volume = node_info[self.node_type_index['volume']] #(float)
            goods_weight = node_info[self.node_type_index['weight']] #(float)
            cur_time = 8*60 #当前时间，8：00换算成分钟(int)
            earliest_time = node_info[self.node_type_index['earliest_time']] #最早到达时间(int)
            latest_time = node_info[self.node_type_index['latest_time']] #最晚到达时间(int)
            dis, time = self.input_distance_time[0][i] if i !=0 else [0,0]
            distance = dis #车与该节点的距离 (int)
            remain_power = self.car[self.carID]['max_power'] #车剩余电量，用剩余里程表示(int)
            if cur_time+time > 24*60 or remain_power - dis < 0:
                arrivable = 0
            else:
                arrivable = 1
            arrived_remain_power = remain_power - dis #(int)
            remain_volume = self.car[self.carID]['max_volume'] #体积, float
            remain_weight = self.car[self.carID]['max_weight'] #重量, float
            embedding = [0 for ii in range(20)]
            s_0[i] = [nodeType, isDeliver, goods_volume, goods_weight, cur_time, \
                        latest_time, earliest_time, distance, remain_power, \
                        arrivable,arrived_remain_power, remain_volume,remain_weight, embedding]
            if nodeType == 2 or nodeType == 3:
                self.remainNodes+= 1
            
        return s_0
    
    def missionIsComplete(self):
        if self.remainNodes > 0:
            return False
        else:
            return True
        
    def getCurrentReward(self):
        return self.finalReward
    
    def get_transition(self,state,action):
        s_next = None
        reward = None
        terminal = None
        
        if state == None or action not in state:
            print('you can not go there')
            reward = -1000
            self.finalReward += reward
            return state,reward,False
        
        if state[action][self.feature_index['arrivable']] == 0:
            reward = -1000
            terminal = True
            self.finalReward += reward
            return state, reward, terminal
        
        if self.missionIsComplete():
            reward = 1000
            #terminal = True
            self.finalReward += reward
            return self.realCost
        
        nodeType = self.input_node_type[action][self.node_type_index['type']]
        try:
            s_next, reward, terminal = self.updateState(nodeType, state, action)
        except:
            import ipdb
            ipdb.set_trace()
        return s_next,reward,terminal

    #---------------------------------------------------------
    def updateState(self, nodeType, state, action):
        terminal = False
        reward = 0
        if action == self.currentNode:
            print('you are here!')
            reward = -1000
            self.finalReward += reward
            return state, reward, terminal
            # print('you are here!')
            # return None, None, None
            return state,-1000,False # this is not sure
        
        if nodeType == 1: #deliverCenter
            dis, time = self.input_distance_time[self.currentNode][action]
            
            if state[action][self.feature_index['remain_volume']] > 0:
                reward = 100 - dis*self.disCost - 60*self.waitCost
                self.realCost += - dis*self.disCost - 60*self.waitCost
            else:
                reward = -dis*self.disCost
                self.realCost += - dis*self.disCost
            for nid, s in state.items():
                remain_p = self.car[self.carID]['max_power']
                
                s[self.feature_index['cur_time']] += time
                s[self.feature_index['remain_power']] = remain_p
                s[self.feature_index['isDeliver']] = 1
                s[self.feature_index['remain_volume']] = self.car[self.carID]['max_volume']
                s[self.feature_index['remain_weight']] = self.car[self.carID]['max_weight']

                if action == nid:
                    s[self.feature_index['arrived_remain_power']] = remain_p
                    s[self.feature_index['distance']] = 0
                else:
                    dis_, time_ = self.input_distance_time[action][nid]
                    s[self.feature_index['arrived_remain_power']] = remain_p - dis_
                    s[self.feature_index['distance']] = dis_
                
                if s[self.feature_index['cur_time']] > 24*60 \
                or s[self.feature_index['arrived_remain_power']] < 0:
                    s[self.feature_index['arrivable']] = 0
                else:
                    s[self.feature_index['arrivable']] = 1
            self.currentNode = action
            self.finalReward += reward
            return state, reward, terminal
        
        elif nodeType == 2 or nodeType == 3: #seller or buyer
            dis, time = self.input_distance_time[self.currentNode][action]
            # the node is delivered
            if state[action][self.feature_index['isDeliver']] == 1:
                for nid ,s in state.items():
                    s[self.feature_index['cur_time']] += time
                    s[self.feature_index['remain_power']] -= dis
                    if action == nid:
                        s[self.feature_index['arrived_remain_power']] -= dis
                        s[self.feature_index['distance']] = 0
                    else:
                        dis_, time_ = self.input_distance_time[action][nid]
                        s[self.feature_index['arrived_remain_power']] -= (dis + dis_)
                        s[self.feature_index['distance']] = dis_
                    if s[self.feature_index['cur_time']] > 24*60 \
                    or s[self.feature_index['arrived_remain_power']] < 0:
                        s[self.feature_index['arrivable']] = 0
                    else:
                        s[self.feature_index['arrivable']] = 1
                reward = -dis*self.disCost
                self.realCost += -dis*self.disCost
            # the node is not delivered
            else:
                remain_volume = state[action][self.feature_index['remain_volume']]
                remain_weight = state[action][self.feature_index['remain_weight']]
                goods_volume = state[action][self.feature_index['goods_volume']]
                goods_weight = state[action][self.feature_index['goods_weight']]
                cur_time = state[action][self.feature_index['cur_time']]
                earliest_time = state[action][self.feature_index['earliest_time']]
                latest_time = state[action][self.feature_index['latest_time']]
                max_power = self.car[self.carID]['max_power']
                
                canGetorPut = False
                # judge whether node can get/put goods
                if nodeType == 2:
                    if (cur_time + time) > latest_time \
                        or (remain_volume<goods_volume or remain_weight<goods_weight):
                            canGetorPut = False
                    elif remain_volume>=goods_volume and remain_weight>=goods_weight:
                            canGetorPut = True
                            self.remainNodes -= 1
                if nodeType == 3:
                    if (cur_time + time) > latest_time \
                        or (max_power-remain_volume<goods_volume or max_power-remain_weight<goods_weight):
                            canGetorPut = False
                    elif (max_power-remain_volume>=goods_volume and max_power-remain_weight>=goods_weight):
                            canGetorPut = True
                            self.remainNodes -= 1
                
                for nid ,s in state.items():
                    s[self.feature_index['cur_time']] += time
                    s[self.feature_index['remain_power']] -= dis

                    if action == nid:
                        s[self.feature_index['arrived_remain_power']] -= dis
                        s[self.feature_index['distance']] = 0
                    else:
                        dis_, time_ = self.input_distance_time[action][nid]
                        s[self.feature_index['arrived_remain_power']] -= (dis + dis_)
                        s[self.feature_index['distance']] = dis_
                    
                    if canGetorPut:
                        if nodeType == 2:
                            s[self.feature_index['remain_volume']] -= goods_volume
                            s[self.feature_index['remain_weight']] -= goods_weight
                        else:
                            s[self.feature_index['remain_volume']] += goods_volume
                            s[self.feature_index['remain_weight']] += goods_weight
                        if action == nid:
                            s[self.feature_index['goods_volume']] = 0.0
                            s[self.feature_index['goods_weight']] = 0.0
                        s[self.feature_index['isDeliver']] = 1
                    
                    if s[self.feature_index['cur_time']] > 24*60 \
                    or s[self.feature_index['arrived_remain_power']] < 0:
                        s[self.feature_index['arrivable']] = 0
                    else:
                        s[self.feature_index['arrivable']] = 1
                if canGetorPut:
                    cost = - dis*self.disCost - (30 + cur_time+time-earliest_time)*self.waitCost
                    reward = 200 + cost
                    self.realCost += cost
                else:
                    reward = -dis*self.disCost
                    self.realCost += cost
            self.currentNode = action
            self.finalReward += reward
            return state,reward,terminal
    
        elif nodeType == 4: #electricPip
            for nid, s in state.items():
                dis, time = self.input_distance_time[self.currentNode][action]
                remain_p = self.car[self.carID]['max_power']
                
                s[self.feature_index['cur_time']] += time
                s[self.feature_index['remain_power']] = remain_p
                s[self.feature_index['isDeliver']] = 1

                if action == nid:
                    s[self.feature_index['arrived_remain_power']] = remain_p
                    s[self.feature_index['distance']] = 0
                else:
                    dis_, time_ = self.input_distance_time[action][nid]
                    s[self.feature_index['arrived_remain_power']] = remain_p - dis_
                    s[self.feature_index['distance']] = dis_
                
                if s[self.feature_index['cur_time']]+time > 24*60 \
                or s[self.feature_index['arrived_remain_power']] < 0:
                    s[self.feature_index['arrivable']] = 0
                else:
                    s[self.feature_index['arrivable']] = 1
            reward = -50 - dis*self.disCost -100
            self.realCost += -dis*self.disCost -100
            self.currentNode = action
            self.finalReward += reward
            return state,reward,terminal

if __name__ == '__main__':
    env = environment(0) 
    s_0 = env.get_start_state()
    print(s_0)
    s_next, reward, terminal = env.get_transition(s_0,50001)
    print(env.realCost)
    print(env.getCurrentReward())
    s_next, reward, terminal = env.get_transition(s_next,50004)
    print(env.realCost)
    print(reward)

    #print(env.missionIsComplete())





