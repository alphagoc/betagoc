#!/usr/bin/python
# -*- coding:utf-8 -*-

#==================================
#@file name: environment
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:8/11/18,8:13 PM
#==================================
from __future__ import print_function

import random
import time as tm
import numpy as np
import pandas as pd
from collections import OrderedDict

#import ipdb

feature_index = {'nodeType':0, 'isDeliver':1, 'goods_volume':2, \
                 'goods_weight':3, 'cur_time':4, 'latest_time':5, \
                 'earliest_time':6, 'distance':7, 'remain_power':8, \
                 'arrivable':9,'arrived_remain_power':10, \
                 'remain_volume':11, 'remain_weight':12, 'embedding':13}
car = [{'max_volume':12.0,'max_weight':2.0,'max_power':100000,'disCost':0.012,'carCost':200},\
       {'max_volume':16.0,'max_weight':2.5,'max_power':120000,'disCost':0.014,'carCost':300}]
node_type_index = {'type':0,'weight':1,'volume':2,'earliest_time':3,'latest_time':4}

input_distance_time_file = ['./data/inputdistancetime_1_1601.txt', \
                            './data/inputdistancetime_2_1501.txt', \
                            './data/inputdistancetime_3_1601.txt', \
                            './data/inputdistancetime_4_1301.txt', \
                            './data/inputdistancetime_5_1201.txt']
waitCost = 0.4
#action_number = 100
actionsMap = {}

def loadData(dataID):
    input_distance_time = {}
    input_node_type = {}
    allNodes = set()
    # load input_distance_time
    with open(input_distance_time_file[dataID],'r') as f:
        line = f.readlines()
        input_dis_time = line[1:]
    for line in input_dis_time:
        items = line.strip().split(',')
        (o, node_1, node_2, dis, time) = list(map(int,items))
        if node_1 not in input_distance_time:
            input_distance_time[node_1] = {}
            allNodes.add(node_1)
        input_distance_time[node_1][node_2] = [dis, time]
    
    # load input_node_type
    node_type_data = np.array(pd.read_csv('./data/inputnodes.csv'))[:,1:]
    for row in node_type_data:
        input_node_type[int(row[0])] = [int(row[1]),row[2],row[3],int(row[4]),int(row[5])]
    
    return input_distance_time, input_node_type, allNodes


def get_actions(state):
    actions = []
    for nid, s in state.items():
        if type(nid) == str:
            continue
        if nid == state['currentNode']:
            continue
        nt = s[feature_index['nodeType']]
        isD = s[feature_index['isDeliver']]
        if nt == 1 or nt == 4 or isD == 0:
            actions.append(nid)
      
    return actions        

def isFinish(state):
    for nid, s in state.items():
        if type(nid) == str:
            continue
        if s[feature_index['isDeliver']] == 0:
            return False
    return True

def get_start_state(carID, state, input_distance_time, input_node_type, allNodes):
    global actionsMap
    if state == None:
        s_0 = OrderedDict()
        nodes = allNodes
        index = 0
        for n in nodes:
            node_info = input_node_type[n]
            
            nodeType = node_info[node_type_index['type']] # 1:配送中心; 4:充电桩; 3:揽货商家 ; 2:送货商家(int)
            if nodeType==2 or nodeType==3:
                isDeliver = 0
            else:
                isDeliver = 1 #是否配送过 (int)
            goods_volume = node_info[node_type_index['volume']] #(float)
            goods_weight = node_info[node_type_index['weight']] #(float)
            cur_time = 8*60 #当前时间，8：00换算成分钟(int)
            earliest_time = node_info[node_type_index['earliest_time']] #最早到达时间(int)
            latest_time = node_info[node_type_index['latest_time']] #最晚到达时间(int)
            dis, time = input_distance_time[0][n] if n !=0 else [0,0]
            distance = dis #车与该节点的距离 (int)
            remain_power = car[carID]['max_power'] #车剩余电量，用剩余里程表示(int)
            if cur_time+time > 24*60 or remain_power - dis < 0:
                arrivable = 0
            else:
                arrivable = 1
            arrived_remain_power = remain_power - dis #(int)
            remain_volume = car[carID]['max_volume'] #体积, float
            remain_weight = car[carID]['max_weight'] #重量, float
            embedding = [0 for i in range(20)]
            
            actionsMap[index] = n
            s_0[index] = [nodeType, isDeliver, goods_volume, goods_weight, cur_time, \
                            latest_time, earliest_time, distance, remain_power, \
                            arrivable,arrived_remain_power, remain_volume,remain_weight, embedding]
            index += 1
        s_0['currentNode'] = 0
        s_0['carID'] = carID
        return s_0
    
    else:
        s_next = OrderedDict()
        for nid,s in state.items():
            if type(nid) == str:
                continue
            cur_time =  8*60
            remain_power = car[carID]['max_power']
            dis, time = input_distance_time[0][actionsMap[nid]] if actionsMap[nid] !=0 else [0,0]
            if cur_time+time > 24*60 or remain_power - dis < 0:
                s[feature_index['arrivable']] = 0
            else:
                s[feature_index['arrivable']] = 1
            s[feature_index['remain_power']] = remain_power
            
            s[feature_index['arrived_remain_power']] = remain_power - dis #(int)
            s[feature_index['remain_volume']] = car[carID]['max_volume'] #体积, float
            s[feature_index['remain_weight']] = car[carID]['max_weight'] #重量, float
            s[feature_index['cur_time']] = cur_time
            s_next[nid] = s
        
        s_next['currentNode'] = 0
        s_next['carID'] = carID
        return s_next 
 
    
def get_transition(state, action,input_distance_time, input_node_type, runType='r'):
    global actionsMap
    s_next = None
    reward = None
    terminal = None
    
    if runType == 'r':
        if state[action][feature_index['arrivable']] == 0:
            reward = -10000
            terminal = True
            return state, reward, terminal
        else:
            nodeType = input_node_type[actionsMap[action]][node_type_index['type']]
            s_next, reward, terminal, realCost = updateState(nodeType, state, action, \
                                               input_distance_time)
            return s_next,reward,terminal
    
    elif runType == 'c':
        terminal = False
        if state[action][feature_index['arrivable']] == 0:
            terminal = True
        
        if isFinish(state):
            if state['currentNode'] == 0:
                return state,0.0,True,True
            else:
                return state,10000,True,True
        
        nodeType = input_node_type[actionsMap[action]][node_type_index['type']]
        s_next, reward, t, realCost = updateState(nodeType, state, action, \
                                               input_distance_time)
        if terminal == True:
            realCost = 0
        return s_next,realCost,terminal,False

#---------------------------------------------------------
def updateState(nodeType, state, action, input_distance_time):
    global actionsMap
    s_next = OrderedDict()
    #remainNodes = len(state)-2
    terminal = False
    reward = 0.0
    realCost = 0.0
    currentNode = actionsMap[state['currentNode']]
    carID = state['carID']
    disCost = car[carID]['disCost']
    
    if nodeType == 1: #deliverCenter
        dis, time = input_distance_time[currentNode][actionsMap[action]]
        
        if state[action][feature_index['remain_volume']] > 0:
            reward = 100 - dis*disCost - 60*waitCost
            realCost += dis*disCost + 60*waitCost
        else:
            reward = -dis*disCost
            realCost += dis*disCost
        for nid, s in state.items():
            if type(nid) == str:
                continue
            remain_p = car[carID]['max_power']
            
            s[feature_index['cur_time']] += time
            s[feature_index['remain_power']] = remain_p
            s[feature_index['remain_volume']] = car[carID]['max_volume']
            s[feature_index['remain_weight']] = car[carID]['max_weight']

            if action == nid:
                s[feature_index['arrived_remain_power']] = remain_p
                s[feature_index['distance']] = 0
            else:
                dis_, time_ = input_distance_time[actionsMap[action]][actionsMap[nid]]
                s[feature_index['arrived_remain_power']] = remain_p - dis_
                s[feature_index['distance']] = dis_
                
            if s[feature_index['cur_time']] > 24*60 \
            or s[feature_index['arrived_remain_power']] < 0:
                s[feature_index['arrivable']] = 0
            else:
                s[feature_index['arrivable']] = 1
            #if s[feature_index['isDeliver']] == 1:
                #remainNodes -= 1
            s_next[nid] = s
        s_next['currentNode'] = action
        s_next['carID'] = carID
        return s_next, reward, terminal, realCost
        
    elif nodeType == 2 or nodeType == 3: #seller or buyer
        dis, time = input_distance_time[currentNode][actionsMap[action]]
        # the node is delivered
        if state[action][feature_index['isDeliver']] == 1:
            for nid ,s in state.items():
                if type(nid) == str:
                    continue
                s[feature_index['cur_time']] += time
                s[feature_index['remain_power']] -= dis
                if action == nid:
                    s[feature_index['arrived_remain_power']] -= dis
                    s[feature_index['distance']] = 0
                else:
                    dis_, time_ = input_distance_time[actionsMap[action]][actionsMap[nid]]
                    s[feature_index['arrived_remain_power']] -= (dis + dis_)
                    s[feature_index['distance']] = dis_
                if s[feature_index['cur_time']] > 24*60 \
                or s[feature_index['arrived_remain_power']] < 0:
                    s[feature_index['arrivable']] = 0
                else:
                    s[feature_index['arrivable']] = 1
                #if s[feature_index['isDeliver']] == 1:
                    #remainNodes -= 1
                s_next[nid] = s
            reward = -dis*disCost
            realCost += dis*disCost
            s_next['currentNode'] = action
            s_next['carID'] = carID
            return s_next, reward, terminal, realCost
        # the node is not delivered
        else:
            remain_volume = state[action][feature_index['remain_volume']]
            remain_weight = state[action][feature_index['remain_weight']]
            goods_volume = state[action][feature_index['goods_volume']]
            goods_weight = state[action][feature_index['goods_weight']]
            cur_time = state[action][feature_index['cur_time']]
            earliest_time = state[action][feature_index['earliest_time']]
            latest_time = state[action][feature_index['latest_time']]
            car_max_volume = car[carID]['max_volume']
            car_max_weight = car[carID]['max_weight']
            
            canGetorPut = False
            # judge whether node can get/put goods
            if nodeType == 2:
                if (cur_time + time) > latest_time \
                or (remain_volume<goods_volume or remain_weight<goods_weight):
                    canGetorPut = False
                elif remain_volume>=goods_volume and remain_weight>=goods_weight:
                    canGetorPut = True
            if nodeType == 3:
                if (cur_time + time) > latest_time \
                or (car_max_volume-remain_volume<goods_volume or car_max_weight-remain_weight<goods_weight):
                    canGetorPut = False
                elif (car_max_volume-remain_volume>=goods_volume and car_max_weight-remain_weight>=goods_weight):
                    canGetorPut = True
                
            for nid ,s in state.items():
                if type(nid) == str:
                    continue
                s[feature_index['cur_time']] += time
                t = s[feature_index['cur_time']]
                s[feature_index['remain_power']] -= dis

                if action == nid:
                    s[feature_index['arrived_remain_power']] -= dis
                    s[feature_index['distance']] = 0
                else:
                    dis_, time_ = input_distance_time[actionsMap[action]][actionsMap[nid]]
                    s[feature_index['arrived_remain_power']] -= (dis + dis_)
                    s[feature_index['distance']] = dis_
                    
                if canGetorPut:
                    if nodeType == 2:
                        s[feature_index['remain_volume']] -= goods_volume
                        s[feature_index['remain_weight']] -= goods_weight
                        s[feature_index['cur_time']] = earliest_time+30 if t < earliest_time else t+30
                    else:
                        s[feature_index['remain_volume']] += goods_volume
                        s[feature_index['remain_weight']] += goods_weight
                        s[feature_index['cur_time']] = earliest_time+30 if t < earliest_time else t+30
                    if action == nid:
                        s[feature_index['goods_volume']] = 0.0
                        s[feature_index['goods_weight']] = 0.0
                        s[feature_index['isDeliver']] = 1
                    
                if s[feature_index['cur_time']] > 24*60 \
                or s[feature_index['arrived_remain_power']] < 0:
                    s[feature_index['arrivable']] = 0
                else:
                    s[feature_index['arrivable']] = 1
                #if s[feature_index['isDeliver']] == 1:
                    #remainNodes -= 1
                s_next[nid] = s
            if canGetorPut:
                cost = - dis*disCost - (30 + cur_time+time-earliest_time)*waitCost
                reward = 200 + cost
                realCost += -cost
            else:
                reward = -dis*disCost
                realCost += -reward
            s_next['currentNode'] = action
            s_next['carID'] = carID
            return s_next,reward,terminal, realCost
    
    elif nodeType == 4: #electricPip
        dis, time = input_distance_time[currentNode][actionsMap[action]]
        for nid, s in state.items():
            if type(nid) == str:
                continue
            remain_p = car[carID]['max_power']
            
            s[feature_index['cur_time']] += time
            s[feature_index['remain_power']] = remain_p

            if action == nid:
                s[feature_index['arrived_remain_power']] = remain_p
                s[feature_index['distance']] = 0
            else:
                dis_, time_ = input_distance_time[actionsMap[action]][actionsMap[nid]]
                s[feature_index['arrived_remain_power']] = remain_p - dis_
                s[feature_index['distance']] = dis_
                
            if s[feature_index['cur_time']] > 24*60 \
            or s[feature_index['arrived_remain_power']] < 0:
                s[feature_index['arrivable']] = 0
            else:
                s[feature_index['arrivable']] = 1
            #if s[feature_index['isDeliver']] == 1:
                #remainNodes -= 1
            s_next[nid] = s
        reward = -50 - dis*disCost -100
        realCost += dis*disCost + 100
        s_next['currentNode'] = action
        s_next['carID'] = carID
        return s_next,reward,terminal,realCost

if __name__ == '__main__':
    def show(reward, cnode, ctype, action, actionType):
        cnode = str(actionsMap[cnode])+'('+str(ctype)+')'
        action = str(actionsMap[action])+'('+str(actionType)+')'
        s = map(str,[reward, cnode, 'to', action])
        print('  '.join(s))
    
    input_distance_time, input_node_type, allNodes = loadData(4)
    
    state = None
    for batch in range(5):
        carID = random.choice([0,1])
        state = get_start_state(carID,state,input_distance_time,input_node_type,allNodes)
        
        while True:
            actions = get_actions(state)
            action = random.choice(actions)
            actionType = state[action][feature_index['nodeType']]
            cnode = state['currentNode']
            ctype = state[cnode][feature_index['nodeType']]
            
            s_next,reward,terminal,f = get_transition(state, action, input_distance_time,input_node_type,runType='c')
            show(reward, cnode, ctype, action, actionType)
            
            if terminal == True:
                state = s_next
                print('end')
                print('==========================')
                break
            state = s_next
            tm.sleep(3)
        
    print(isFinish(state))
    