#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: train
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:8/17/18,8:39 AM
#==================================

import numpy as np
import ipdb
import argparse
import util
import tensorflow as tf
import os
from train_pipe import train_pipe


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

@util.Pipe
def parse_arguments(*args):
    parser = argparse.ArgumentParser(description='Mcts for Goc')
    parser.add_argument('-model',dest='model',type=str,default="Training",help="the running model")
    parser.add_argument('-task',dest='task',type=str,default="train",help="train or evaluate")
    parser.add_argument('-log_path', dest='log_path',type=str,default='./logs',help='the path for storing the log files')
    parser.add_argument('-no_gpu',dest='no_gpu',type=str,default='0',help='which GPU is used for running the code')
    parser.add_argument('-training_epoch',dest='training_epoch',type=int,default=1000,help='the training epoch number')
    parser.add_argument('-batch_size',dest='batch_size',type=int,default=1024,help='the batch size')
    parser.add_argument('-buffer_size',dest='buffer_size',type=int,default=100000,help='the max buffer size')
    parser.add_argument('-restore_model',dest='restore_model',type=str2bool,default='False',help='load model from files')
    parser.add_argument('-state_size',dest='state_size',type=int,default=65,help='setting the embedding size')
    parser.add_argument('-action_size',dest='action_size',type=int,default=200,help='the node number')
    parser.add_argument('-simulation_num',dest='simulation_num',type=int,default=1000,help='the simulation number before update')
    parser.add_argument('-temperature',dest='temperature',type = float,default=1.0,help='the temperature parameter')
    parser.add_argument('-n_playout',dest='n_playout',type = int,default=2000,help='mc search number')
    parser.add_argument('-c_puct',dest='c_puct',type = int,default=5,help='c_puct number')
    parser.add_argument('-discount_factor',dest='discount_factor',type = float,default=0.9,help='c_puct number')
    parser.add_argument('-learning_rate',dest='learning_rate',type = float,default=0.01,help='learning rate tensorflow')
    parser.add_argument('-kl_targ',dest='kl_targ',type = float,default=0.02,help='kl divergence')
    parser.add_argument('-mini_epoch',dest='mini_epoch',type = int,default=10,help='mini_epoch')
    parser.add_argument('-lr_multiplier',dest='lr_multiplier',type = float,default=0.1,help='lr_multiplier')
    parser.add_argument('-node_number',dest='node_number',type = int,default=200,help='node_number')
    parser.add_argument('-simulator',dest='simulator',type = int,default=10,help='the simulator num')
    parser.add_argument('-saved_model_path',dest='saved_model_path',type = str,default="saved_model",help='save_model_path')
    parser.add_argument('-car_type',dest='car_type',type = int,default=0,help='the training car_type')
    parser.add_argument('-car_one_save_path',dest='car_one_save_path',type = str,default='./car_one_0',help='save_model_path')
    parser.add_argument('-car_zero_save_path',dest='car_zero_save_path',type = str,default='./car_zero_0',help='save_model_path')
    args = parser.parse_args()
    args.GPU_OPTION = tf.GPUOptions(allow_growth=True)
    return args


@util.Pipe
def initialize(config,*args):
    if not os.path.isdir(config.log_path):
        os.mkdir(config.log_path)
    util.log.set_log_path(os.path.join(config.log_path,config.model)+"_"+str(util.get_now_time())+".log")
    util.log.info('saving log file in '+util.log().log_path)
    util.log.structure_info("config for experiments",list(vars(config).items()))
    return config


if __name__ == '__main__':
    config = []|parse_arguments|initialize
    os.environ["CUDA_VISIBLE_DEVICES"] = str(config.no_gpu)
    if config.task=="train":
        train_pipe(config).run()
    if config.task =="evaluate":
        train_pipe(config).evaluate()