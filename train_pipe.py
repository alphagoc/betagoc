#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: train_pipe
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:8/17/18,8:59 AM
#==================================

import numpy as np
import ipdb
import random
from environment import *
from env_mcts import dispathcher_simulator
from collections import defaultdict, deque
from MCTS import MCTSdispatcher
from policy_value_network import policy_value_network
from util.zlog import log
import copy as cp
import tensorflow as tf
import os
from multiprocessing import Process, Queue, Lock,Manager
import time
def policy_value_function_sample(state):
    acts = []
    for key in get_actions(state):
        acts.append((key,0.000625))
    return acts,0.0

def create_training_samples(name,mc_dispatcher,dispatch_simulator,temperature,lock,random_seed,training=[]):
    np.random.seed(random_seed)
    print("start process " + str(name))
    training_data = list(dispatch_simulator.start_dispathcher(mc_dispatcher,temperature))[:]
    with lock:
        training.extend(training_data)
    print("finish process " + str(name))

def save_model(saver, path, sess, global_step):
    temp = path[:str(path).rfind('/')]
    os.system("rm " + temp + '/*')
    if not os.path.isdir(temp):
        os.mkdir(temp)
    saver.save(sess, path, global_step)

def create_saved_path(index,save_root_path,save_sub_path):
    return os.path.join(os.path.join(save_root_path,
                        save_sub_path+"_"+str(index)),
                        save_sub_path+"_"+str(index))
def load_model(saver,sess,path):
    saver.restore(sess, tf.train.latest_checkpoint(path))
    pass


class train_pipe(object):
    SAVE_TIME = 0
    def save_agent(self,save_time):
        log.info("SAVE AGENT",save_time)
        save_model(self.agent.saver,
                        create_saved_path(save_time,self.config.saved_model_path,
                                               self.config.model),
                        self.agent.sess,
                        self.agent.global_step)

    def load_model(self,save_time):
        path = os.path.join(self.config.saved_model_path,self.config.model+"_"+str(save_time))
        self.agent.saver.restore(self.agent.sess,tf.train.latest_checkpoint(path))


    def __init__(self,config):
        self.config = config
        self.data_buffer = deque(maxlen=self.config.buffer_size)
        self.data_info = loadData(0)
        self.mc_dispatchers = []
        self.dispatch_simulators = []
        self.lock = Lock()
        if self.config.task=="train":
            self.agent = policy_value_network(self.config,variable_scope=self.config.model)
            for i in range(self.config.simulator):
                self.mc_dispatchers.append(MCTSdispatcher(policy_value_function_sample, self.config.c_puct, self.config.n_playout))
                self.dispatch_simulators.append(dispathcher_simulator(self.config,self.data_info))
        else:
            self.mc_dispatcher = MCTSdispatcher(policy_value_function_sample, self.config.c_puct, self.config.n_playout)
            self.dispatch_simulator = dispathcher_simulator(self.config,self.data_info)

    def collect_training_data(self):
        for i in range(self.config.simulation_num):
            pw = []
            training_data = Manager().list()
            for j in range(self.config.simulator):
                seed = np.random.choice(range(1000000000))
                pw.append(Process(target=create_training_samples, args=(
                j, self.mc_dispatchers[j], self.dispatch_simulators[j], self.config.temperature, self.lock, seed, training_data)))
                pw[-1].start()
            for j in range(self.config.simulator):
                pw[j].join()
            self.data_buffer.extend(training_data)
            log.info("size of data buffer",len(self.data_buffer))
        pass

    def policy_update(self):
        """update the policy-value net"""
        mini_batch = random.sample(self.data_buffer, self.config.batch_size)
        state_batch = [data[0] for data in mini_batch]
        mcts_probs_batch = [data[1] for data in mini_batch]
        reward_batch = [data[2] for data in mini_batch]
        old_probs, old_v = self.agent.batch_policy_value_fn(state_batch)
        for i in range(self.config.mini_epoch):
            loss,v_loss,a_loss = self.agent.optimize_model(state_batch,mcts_probs_batch,reward_batch,
                                                           self.config.learning_rate*self.config.lr_multiplier)
            log.info("total loss",loss,'value loss',v_loss,'entropy loss',a_loss)
            new_probs, new_v = self.agent.batch_policy_value_fn(state_batch)
            kl = np.mean(np.sum(old_probs * (
                np.log(old_probs + 1e-10) - np.log(new_probs + 1e-10)),
                                axis=1)
                         )
            if kl > self.config.kl_targ * 4:  # early stopping if D_KL diverges badly
                break
        if kl > self.config.kl_targ * 2 and self.config.lr_multiplier > 0.1:
            self.config.lr_multiplier /= 1.5
        elif kl < self.config.kl_targ / 2 and self.config.lr_multiplier < 10:
            self.config.lr_multiplier *= 1.5
        explained_var_old = (1 -np.var(np.array(reward_batch) - old_v.flatten()) /
                                np.var(np.array(reward_batch)))
        explained_var_new = (1 -np.var(np.array(reward_batch) - new_v.flatten()) /
                                 np.var(np.array(reward_batch)))
        log.info(("kl:{:.5f},"
               "lr_multiplier:{:.3f},"
               "loss:{},"
               "entropy:{},"
               "explained_var_old:{:.3f},"
               "explained_var_new:{:.3f},"
               "mean_reward:{:.3f}").format(kl,self.config.lr_multiplier,v_loss,a_loss,explained_var_old,explained_var_new,np.mean(reward_batch)))
        self.save_agent(self.SAVE_TIME)
        self.SAVE_TIME+=1
        return v_loss, a_loss

    def run(self):
        self.save_agent(self.SAVE_TIME)
        try:
            for i in range(self.config.training_epoch):
                self.collect_training_data()
                if len(self.data_buffer)>self.config.batch_size:
                    self.policy_update()
        except KeyboardInterrupt:
            print('\n\rquit')
        pass

    def evaluate(self):
        self.agent_zero = policy_value_network(self.config,variable_scope="car_zero")
        self.agent_one = policy_value_network(self.config,variable_scope="car_one")
        load_model(self.agent_one.saver,self.agent_one.sess,os.path.join(self.config.saved_model_path,self.config.car_one_save_path))
        load_model(self.agent_zero.saver,self.agent_zero.sess,os.path.join(self.config.saved_model_path,self.config.car_zero_save_path))
        total_reward, unreachable_cost, car_num = self.evaluate_model()
        log.info("total reward", total_reward, "unreachable time", unreachable_cost, "car_num", car_num)
        pass
        # saved_model = os.listdir(os.path.abspath(self.config.saved_model_path))
        # saved_model = [int(item.split("_")[-1]) for item in saved_model if item.__contains__(self.config.model)]
        # saved_model.sort()
        # processing = []
        # while True:
        #     for num in saved_model:
        #         if num in processing:
        #             continue
        #         if not num % 1 == 0: continue
        #         total_reward,unreachable_cost,car_num_0 = self.evaluate_model(num)
        #         # ndcg, alpha_ndcg, f1 ,values= self.evaluate_model_mc(num)
        #         processing.append(num)
        #         log.info("total reward",total_reward,"unreachable time",unreachable_cost,"car_num",car_num_0)
        #         total_reward,unreachable_cost,car_num_0 = self.evaluate_mc(num)
        #         log.info("total reward",total_reward,"unreachable_cost",unreachable_cost,"car_num",car_num_0)
        #     time.sleep(5)
        #     saved_model = os.listdir(os.path.abspath(self.config.saved_model_path))
        #     saved_model = [int(item.split("_")[-1]) for item in saved_model if item.__contains__(self.config.model)]
        #     saved_model.sort()

    def evaluate_model(self):
        finish = False
        current_state =  None
        total_costs = 0
        car_number = [0,0]
        unreachable_cost = 0
        while not finish:
            terminal = False
            current_state_0 = get_start_state(0,current_state,self.data_info[0],self.data_info[1],self.data_info[2])
            current_state_1 = get_start_state(1,current_state,self.data_info[0],self.data_info[1],self.data_info[2])
            while not terminal:
                ### with car 0
                s_next_0,cost_0,terminal_0,finish_0 = self.chose_action_policy_value(current_state_0,0)
                s_next_1,cost_1,terminal_1,finish_1 = self.chose_action_policy_value(current_state_1,1)
                if cost_0!=0 and cost_1!=0:
                    if cost_0<cost_1:
                        which_car = 0
                        current_state = s_next_0
                        terminal = terminal_0
                        finish = finish_0
                        cost = cost_0
                    else:
                        which_car = 1
                        current_state = s_next_1
                        terminal = terminal_1
                        finish = finish_1
                        cost = cost_1
                elif cost_0==0:
                    which_car = 1
                    current_state = s_next_1
                    terminal = terminal_1
                    finish = finish_1
                    cost = cost_1
                elif cost_1==0:
                    which_car = 0
                    current_state = s_next_0
                    terminal = terminal_0
                    finish = finish_0
                    cost = cost_0
                if cost ==0:
                    unreachable_cost+=1
                else:
                    total_costs+=cost
                if terminal:
                    car_number[which_car]+=1
            finish = isFinish(current_state)
            if sum(car_number)>2*self.config.action_size:
                finish = True
        return total_costs,unreachable_cost,car_number

    def chose_action_policy_value(self,state,num):
        if num ==0:
            action, value_fn = self.agent_zero.policy_value_fn(state)
        elif num==1:
            action, value_fn = self.agent_one.policy_value_fn(state)
        actions, action_prob = zip(*action)
        action_prob = np.asarray(action_prob) + 0.01
        act = np.random.choice(actions, p=1 / sum(action_prob) * action_prob)
        s_next, realCost, terminal, finish = get_transition(state, act, self.data_info[0],
                                                                   self.data_info[1], 'c')
        return s_next,realCost,terminal,finish

    # def choose_action_mc(self,state):
    #     action, mc_policy = self.mc_dispatcher.get_action(cp.deepcopy(state), temp=self.config.temperature,
    #                                                       return_prob=1,
    #                                                       training=True, data_info=self.data_info)
    #     s_next, realCost, terminal, finish = get_transition(state, action, self.data_info[0],
    #                                                                self.data_info[1], 'c')
    #     return s_next,realCost,terminal,finish

    # def evaluate_mc(self,num):
    #     self.load_model(num)
    #     finish = False
    #     current_state =  None
    #     total_costs = 0
    #     car_number = [0,0]
    #     unreachable_cost = 0
    #     which_car =0
    #     while not finish:
    #         terminal = False
    #         current_state_0 = get_start_state(0, current_state, self.data_info[0], self.data_info[1], self.data_info[2])
    #         current_state_1 = get_start_state(1, current_state, self.data_info[0], self.data_info[1], self.data_info[2])
    #         while not terminal:
    #             ### with car 0
    #             s_next_0, cost_0, terminal_0, finish_0 = self.choose_action_mc(current_state_0)
    #             s_next_1, cost_1, terminal_1, finish_1 = self.choose_action_mc(current_state_1)
    #             if cost_0 != 0 and cost_1 != 0:
    #                 if cost_0 < cost_1:
    #                     which_car = 0
    #                     current_state = s_next_0
    #                     terminal = terminal_0
    #                     finish = finish_0
    #                     cost = cost_0
    #                 else:
    #                     which_car = 1
    #                     current_state = s_next_1
    #                     terminal = terminal_1
    #                     finish = finish_1
    #                     cost = cost_1
    #             elif cost_0 == 0:
    #                 which_car = 1
    #                 current_state = s_next_1
    #                 terminal = terminal_1
    #                 finish = finish_1
    #                 cost = cost_1
    #             elif cost_1 == 0:
    #                 which_car = 0
    #                 current_state = s_next_0
    #                 terminal = terminal_0
    #                 finish = finish_0
    #                 cost = cost_0
    #             if cost == 0:
    #                 unreachable_cost += 1
    #             else:
    #                 total_costs += cost
    #             if terminal:
    #                 car_number[which_car] += 1
    #         finish = isFinish(current_state)
    #         if sum(car_number) > 2 * self.config.action_size:
    #             finish = True
    #     return total_costs,unreachable_cost,car_number






