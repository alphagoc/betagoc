#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: env_mcts
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:8/17/18,2:42 PM
#==================================

import numpy as np
import ipdb
from environment import *
from MCTS import MCTSdispatcher
import copy as cp


def convert_state2vector(state={},action_size=1601):
    # Currently, the network embedding isn't used
    state = [state[i] for i in range(action_size)]
    node_size = len(state[0][:-1]) + 3 + len(state[0][-1])
    emb_state = np.zeros((len(state), node_size))
    for i, item in enumerate(state):
        emb_state[i, item[0] - 1] = 1  # which node
        emb_state[i, 4:4 + len(item[1:-1])] = np.asarray(item[1:-1])
        emb_state[i, 4 + len(item[1:-1]):] = np.asarray(item[-1])  # set other factors
    return emb_state

def get_action_size(state):
    return len(state)-1

class dispathcher_simulator(object):
    def __init__(self,config,data_info):
        self.config = config
        self.state = None
        self.data_info = data_info # input_distance_time, input_node_type, allNodes

    def start_dispathcher(self,dispathcher,temperature=0.0001):
        if self.state!=None and isFinish(self.state):
            self.state = None
        car_type = self.config.car_type
        current_state = get_start_state(car_type, self.state,self.data_info[0],self.data_info[1],self.data_info[2])
        states,mcts_prob,mcts_actions,trajectory_reward = [],[],[],[]
        while True:
            action, action_prob = dispathcher.get_action(cp.deepcopy(current_state), temp=temperature, return_prob=1,
                                                         training=True,data_info = self.data_info)
            states.append(current_state)
            mcts_prob.append(action_prob)
            mcts_actions.append(action)
            try:
                current_state,reward,terminal = get_transition(current_state,action,self.data_info[0],self.data_info[1], runType='r') # s_next,reward,terminal
                trajectory_reward.append(reward)
                if terminal:
                    discounted_reward = []
                    r = trajectory_reward[-1]
                    for rr in trajectory_reward[::-1][1:]:
                        discounted_reward.append(r)
                        r = rr+r * self.config.discount_factor
                    discounted_reward.append(r)
                    self.state = current_state
                    return zip(states, mcts_prob, discounted_reward[::-1])
            except:
                print("encouter error")
                return zip([], [], [])


